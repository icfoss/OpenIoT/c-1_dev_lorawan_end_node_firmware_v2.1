### **C-1_dev_LoRaWAN_end_node_firmware_v2.1**


### *Prerequisite*
----------------
- [STM32 Cube IDE](https://www.st.com/en/development-tools/stm32cubeide.html)
- [C-1 Dev v1.0](https://gitlab.com/icfoss/OpenIoT/c1_dev_v1.0)

### *Getting Started*
-------------------
- Create a workspace inside STM32 Cube IDE
- Import the project to the workspace

![image-2.png](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_end_node_firmware_v2.1/-/raw/main/Images/image-2.png)

- Build project in the toolchain

![image-1.png](https://gitlab.com/abdularshadvs/c-1_dev_lorawan_end_node_firmware_v2.1/-/raw/main/Images/Image-1.png?ref_type=heads)

- Configure the keys and address in se-identity.h
```c
// end-device IEEE EUI (big endian)
#define LORAWAN_DEVICE_EUI                  { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

// App/Join server IEEE EUI (big endian)
#define LORAWAN_JOIN_EUI                    { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

// Set the device address on the network (big endian)
#define LORAWAN_DEVICE_ADDRESS              ( uint32_t )0x00000000
```
***NOTE***
- *For OTA devices, set the APP root key and leave others blank*
- *For ABP devices, set the Network root key,Network session key and Application session key*
- *For LoRaWAN version 1.0.x use same key for Network root  key and Network session key*

```c
// Application root key
#define LORAWAN_APP_KEY                     00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00

// Network root key
#define LORAWAN_NWK_KEY                     00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00

// Forwarding Network session key
#define LORAWAN_NWK_S_KEY                   00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00

// Application session key
#define LORAWAN_APP_S_KEY                   00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00
```
- Set the Transmission Interval and Activation type (ABP/OTA) in lora_app.h
- Write the corresponding source and header file for the sensors
- Include the sensor header in lora_app.c and sys_app.c
- Inside sys_app.c call the initialisation function for the sensors inside SystemApp_Init()
- Inside lora_app.c initialize the structure to hold sensor data, call the function to read sensor data and assign the data in the application buffer for transmission  in SendTxData()
- For interrupt related features, make sure to add the IRQ Handler inside ***stm32l0xx_it.c*** file
```sh
# For UART Configuration select
Baudrate - 115200, 8b, 1 stopbit, no parity, no flow control
```

<pre><code>                                  V  V            
             -------------------  |  |  -------------------
             |    LoRa Object  |  |  |  |   LoRa Network  |
             |                 |  |  |  |                 |
ComPort &lt;--- |                 |--|  |--|                 | --&gt; Web Server         
             |                 |        |                 |
             |                 |        |                 |
             -------------------        -------------------
</code></pre>
                 
                        
### *Application - LoRaWAN End_Node*

### *Directory contents*

---------------------------------------------------------------------------------------------------------
| Filename                                                                     |Description             |
|------------------------------------------------------------------------------|------------------------|
|LoRaWAN_End_Node/Core/Inc/adc.h                                               |This file contains all the function prototypes for the adc.c file|
|LoRaWAN_End_Node/Core/Inc/adc_if.h                                            |Header for ADC interface configuration|
|LoRaWAN_End_Node/Core/Inc/dma.h                                               |This file contains all the function prototypes for the dma.c file|
|LoRaWAN_End_Node/Core/Inc/main.h                                              |Header for main.c file.|
|                                                                              |This file contains the common defines of the application.|
|LoRaWAN_End_Node/Core/Inc/platform.h                                          |Header for General HW instances configuration|
|LoRaWAN_End_Node/Core/Inc/rtc.h                                               |This file contains all the function prototypes for the rtc.c file|
|LoRaWAN_End_Node/Core/Inc/rtc_if.h                                            |configuration of the rtc_if.c instances|
|LoRaWAN_End_Node/Core/Inc/stm32l0xx_hal_conf.h                                |HAL configuration file.|
|LoRaWAN_End_Node/Core/Inc/stm32l0xx_it.h                                      |This file contains the headers of the interrupt handlers.|
|LoRaWAN_End_Node/Core/Inc/stm32_lpm_if.h                                      |Header for Low Power Manager interface configuration|
|LoRaWAN_End_Node/Core/Inc/sys_app.h                                           |Function prototypes for sys_app.c file|
|LoRaWAN_End_Node/Core/Inc/sys_conf.h                                          |Applicative configuration, e.g. : debug, trace, low power, sensors|
|LoRaWAN_End_Node/Core/Inc/sys_debug.h                                         |Configuration of the debug.c instances|
|LoRaWAN_End_Node/Core/Inc/sys_sensors.h                                       |Header for sensors application|
|LoRaWAN_End_Node/Core/Inc/usart.h                                             |This file contains all the function prototypes for the usart.c file|
|LoRaWAN_End_Node/Core/Inc/usart_if.h                                          |Header for USART interface configuration|
|LoRaWAN_End_Node/Core/Inc/utilities_conf.h                                    |Header for configuration file to utilities|
|LoRaWAN_End_Node/Core/Inc/utilities_def.h                                     |Definitions for modules requiring utilities|
|LoRaWAN_End_Node/LoRaWAN/App/app_lorawan.h                                    |Header of application of the LRWAN Middleware|
|LoRaWAN_End_Node/LoRaWAN/App/CayenneLpp.h                                     |Implements the Cayenne Low Power Protocol|
|LoRaWAN_End_Node/LoRaWAN/App/Commissioning.h                                  |End-device commissioning parameters|
|LoRaWAN_End_Node/LoRaWAN/App/lora_app.h                                       |Header of application of the LRWAN Middleware|
|LoRaWAN_End_Node/LoRaWAN/App/lora_app_version.h                               |Definition the version of the application
|LoRaWAN_End_Node/LoRaWAN/App/se-identity.h                                    |Secure Element identity and keys
|LoRaWAN_End_Node/LoRaWAN/Target/b_l072z_lrwan1_bus.h                          |header file for the BSP BUS IO driver
|LoRaWAN_End_Node/LoRaWAN/Target/b_l072z_lrwan1_errno.h                        |Error Code
|LoRaWAN_End_Node/LoRaWAN/Target/cmwx1zzabz_0xx_conf.h                         |This file provides code for the configuration of the shield instances (pin mapping).|
|LoRaWAN_End_Node/LoRaWAN/Target/iks01a2_conf.h                                |This file contains definitions for the MEMS components bus interfaces|
|LoRaWAN_End_Node/LoRaWAN/Target/iks01a3_conf.h                                |This file contains definitions for the MEMS components bus interfaces|
|LoRaWAN_End_Node/LoRaWAN/Target/lorawan_conf.h                                |Header for LoRaWAN middleware instances|
|LoRaWAN_End_Node/LoRaWAN/Target/mw_log_conf.h                                 |Configure (enable/disable) traces|
|LoRaWAN_End_Node/LoRaWAN/Target/radio_board_if.h                              |Header for Radio interface configuration|
|LoRaWAN_End_Node/LoRaWAN/Target/radio_conf.h                                  |Header of Radio configuration|
|LoRaWAN_End_Node/LoRaWAN/Target/stm32l0xx_nucleo_conf.h                       |Configuration file|
|LoRaWAN_End_Node/LoRaWAN/Target/systime.h                                     |Map middleware systime|
|LoRaWAN_End_Node/LoRaWAN/Target/timer.h                                       |Wrapper to timer server|
|LoRaWAN_End_Node/Core/Src/adc.c                                               |This file provides code for the configuration of the ADC instances.|
|LoRaWAN_End_Node/Core/Src/adc_if.c                                            |Read status related to the chip (battery level, VREF, chip temperature)|
|LoRaWAN_End_Node/Core/Src/dma.c                                               |This file provides code for the configuration of all the requested memory to memory DMA transfers.|
|LoRaWAN_End_Node/Core/Src/main.c                                              |Main program body|
|LoRaWAN_End_Node/Core/Src/rtc.c                                               |This file provides code for the configuration of the RTC instances.|
|LoRaWAN_End_Node/Core/Src/rtc_if.c                                            |Configure RTC Alarm, Tick and Calendar manager|
|LoRaWAN_End_Node/Core/Src/stm32l0xx_hal_msp.c                                 |This file provides code for the MSP Initialization and de-Initialization codes.|
|LoRaWAN_End_Node/Core/Src/stm32l0xx_it.c                                      |Interrupt Service Routines.|
|LoRaWAN_End_Node/Core/Src/stm32_lpm_if.c                                      |Low layer function to enter/exit low power modes (stop, sleep)|
|LoRaWAN_End_Node/Core/Src/system_stm32l0xx.c                                  |CMSIS Cortex-M0+ Device Peripheral Access Layer System Source File.|
|LoRaWAN_End_Node/Core/Src/sys_app.c                                           |Initializes HW and SW system entities (not related to the radio)|
|LoRaWAN_End_Node/Core/Src/sys_debug.c                                         |Enables 4 debug pins for internal signals RealTime debugging|
|LoRaWAN_End_Node/Core/Src/sys_sensors.c                                       |Manages the sensors on the application|
|LoRaWAN_End_Node/Core/Src/usart.c                                             |This file provides code for the configuration of the USART instances|
|LoRaWAN_End_Node/Core/Src/usart_if.c                                          |Configuration of UART MX driver interface for hyperterminal communication|
|LoRaWAN_End_Node/LoRaWAN/App/app_lorawan.c                                    |Application of the LRWAN Middleware|
|LoRaWAN_End_Node/LoRaWAN/App/CayenneLpp.c                                     |Implements the Cayenne Low Power Protocol|
|LoRaWAN_End_Node/LoRaWAN/App/lora_app.c                                       |Application of the LRWAN Middleware|
|LoRaWAN_End_Node/LoRaWAN/App/lora_info.c                                      |To give info to the application about LoRaWAN configuration|
|LoRaWAN_End_Node/LoRaWAN/Target/b_l072z_lrwan1_bus.c                          |source file for the BSP BUS IO driver|


### *Acknowledgement*
-------------------
- The [st.com](https://www.st.com/en/embedded-software/i-cube-lrwan.html)  i-cube lrwan project was used in this project
